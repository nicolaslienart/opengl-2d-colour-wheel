# OpenGl 2D colour wheel

Display an HSV colour wheel to be used as a colour picker in OpenGL

## Usage
Use this to pick a colour on click
glReadPixels(e.button.x, WINDOW_HEIGHT - e.button.y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, &currentColour);

## Output
![Program output](colour-wheel-opengl.png)

## References
For further information on HSV please visit : 
[Wikipedia - HSL and HSV](https://en.wikipedia.org/wiki/HSL_and_HSV "Learn more about HSV")
