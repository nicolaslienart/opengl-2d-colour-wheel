#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Used to store output rgb colours from colour_HSVtoRGB */
typedef struct VertexColour {
	GLubyte r;
	GLubyte g;
	GLubyte b;
} VertexColour;


/* @brief Convert HSV entry to RGB
 * 
 * @param[in] rgb The colour structure to be filled as the result
 * @params[in] h,s,v Hue, saturation and value (luminosity)
 *
 * @param[out] translated RGB value is put in pointer rgb
 */
void colour_HSVtoRGB(VertexColour *rgb, float hue, float saturation, float value) {
	float r, g, b;
	float matchValue;

	float chroma = saturation * value;
	float hueSplit = hue / 60.;
	float absoluteValue = (fmod(hueSplit, 2)) - 1; // fmod returns absolute value. Prototype : double fmod(double x, double y);

	if (absoluteValue < 0) absoluteValue = -absoluteValue;

	float x = chroma * (1 - absoluteValue);		

	if (0 <= hueSplit && hueSplit < 1) {
		r = chroma;
		g = x;
		b = 0;
	} else if (1 <= hueSplit && hueSplit < 2) {
		r = x;
		g = chroma;
		b = 0;
	} else if (2 <= hueSplit && hueSplit < 3) {
		r = 0;
		g = chroma;
		b = x;
	} else if (3 <= hueSplit && hueSplit < 4) {
		r = 0;
		g = x;
		b = chroma;
	} else if (4 <= hueSplit && hueSplit < 5) {
		r = x;
		g = 0;
		b = chroma;
	} else if (5 <= hueSplit && hueSplit <= 6) {
		r = chroma;
		g = 0;
		b = x;
	}

	matchValue = value - chroma;
	rgb->r = (matchValue + r) * 255;
	rgb->g = (matchValue + g) * 255;
	rgb->b = (matchValue + b) * 255;
}


/** @brief Print a colour picker wheel
 *
 * @param[in] totalAngles Defines the total precision of the wheel
 */
void artboard_drawColorWheel(int totalAngles) {

	if (totalAngles > 360) {
		totalAngles = 360;
	}

	double angleStep = 2 * M_PI / totalAngles;
	double currentAngle = 0;
	float currentAngleDeg;

	glBegin(GL_TRIANGLE_FAN);
	glColor3ub(255,255,255);
	VertexColour currentColour;
	glVertex2f(0,0);

	while (currentAngle < 2 * M_PI) {
		currentAngleDeg = currentAngle * (180 / M_PI);
		colour_HSVtoRGB(&currentColour, currentAngleDeg, 1, 1);	
		glColor3ub(currentColour.r, currentColour.g, currentColour.b);
		glVertex2f(0.8 * cos(currentAngle), 0.8 * sin(currentAngle) + 0.1);
		currentAngle += angleStep;
	}
	glEnd();

	glBegin(GL_QUADS);
	glColor3ub(0,0,0);
	glVertex2f(1,-0.8);
	glVertex2f(1,-1);
	glColor3ub(255,255,255);
	glVertex2f(-1,-1);
	glVertex2f(-1,-0.8);
	glEnd();
}


/* Dimensions de la fenetre */
static unsigned int WINDOW_WIDTH = 800;
static unsigned int WINDOW_HEIGHT = 600;

/* Nombre de bits par pixel de la fenetre */
static const unsigned int BIT_PER_PIXEL = 32;

/* Nombre minimal de millisecondes separant le rendu de deux images */
static const Uint32 FRAMERATE_MILLISECONDS = 1000 / 60;

int main(int argc, char** argv) 
{
	/* Initialisation de la SDL */
	if(SDL_Init(SDL_INIT_VIDEO) == -1) {
		fprintf(stderr,	"Impossible d'initialiser la SDL. Fin du programme.\n");
		return EXIT_FAILURE;
	}

	/* Ouverture d'une fenetre et creation d'un contexte OpenGL */
	SDL_Surface* surface;
	surface = SDL_SetVideoMode(
			WINDOW_WIDTH, WINDOW_HEIGHT, BIT_PER_PIXEL, 
			SDL_OPENGL | SDL_RESIZABLE | SDL_GL_DOUBLEBUFFER);
	if(NULL == surface) 
	{
		fprintf(stderr, "Impossible d'ouvrir la fenetre. Fin du programme.\n");
		return EXIT_FAILURE;
	}

	/* Boucle principale */
	GLboolean loop = GL_TRUE;
	
	while(loop) 
	{
		/* Recuperation du temps au debut de la boucle */
		Uint32 startTime = SDL_GetTicks();

		glClear(GL_COLOR_BUFFER_BIT);
		
		/* Print colour wheel on screen */
		artboard_drawColorWheel(360);


		SDL_Event e;
		while(SDL_PollEvent(&e)) 
		{
			/* L'utilisateur ferme la fenetre : */
			if(e.type == SDL_QUIT) 
			{
				loop = GL_FALSE;
				break;
			}
		}

		SDL_GL_SwapBuffers();

		/* Calcul du temps ecoule */
		Uint32 elapsedTime = SDL_GetTicks() - startTime;
		/* Si trop peu de temps s'est ecoule, on met en pause le programme */
		if(elapsedTime < FRAMERATE_MILLISECONDS) 
		{
			SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
		}
	}

	/* Liberation des ressources associees a la SDL */ 
	SDL_Quit();

	return EXIT_SUCCESS;
}
